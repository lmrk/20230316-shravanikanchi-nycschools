package com.example.mynycschools;

import androidx.arch.core.executor.testing.InstantTaskExecutorRule;

import com.example.mynycschools.models.NYCSchool;
import com.example.mynycschools.repository.SchoolRepository;
import com.example.mynycschools.viewmodel.SchoolListViewModel;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.rxjava3.android.plugins.RxAndroidPlugins;
import io.reactivex.rxjava3.annotations.NonNull;
import io.reactivex.rxjava3.core.Scheduler;
import io.reactivex.rxjava3.core.Single;
import io.reactivex.rxjava3.internal.schedulers.ExecutorScheduler;
import io.reactivex.rxjava3.plugins.RxJavaPlugins;

public class NYCSchoolsTest {

    @Rule
    public InstantTaskExecutorRule rule = new InstantTaskExecutorRule();

    @Mock
    SchoolRepository schoolRepository;

    @InjectMocks
    SchoolListViewModel schoolListViewModel;

    private Single<List<NYCSchool>> testSingle;

    @Before
    public void setUp(){
        MockitoAnnotations.openMocks(this);
    }

    @Before
    public void setUpRXSchedulers(){
        Scheduler immediate = new Scheduler() {
            @Override
            public @NonNull Worker createWorker() {
                return  new ExecutorScheduler.ExecutorWorker(runnable -> { runnable.run();},true ,true);
            }
        };
        RxJavaPlugins.setInitNewThreadSchedulerHandler(scheduler -> immediate);
        RxJavaPlugins.setInitIoSchedulerHandler(scheduler -> immediate);
        RxAndroidPlugins.setInitMainThreadSchedulerHandler(schedulerCallable -> immediate );
    }

    @Test
    public void getSchoolsListSuccess(){

        NYCSchool school = new NYCSchool("testDBN","testSchool","testBoro",
                "testOverview","testCampus","testEmail","testLoc");
        List<NYCSchool> schoolsList = new ArrayList<>();
        schoolsList.add(school);

        testSingle = Single.just(schoolsList);
        schoolListViewModel = new SchoolListViewModel(schoolRepository);
        Mockito.when(schoolRepository.getSchoolsList()).thenReturn(testSingle);

        schoolListViewModel.refresh();

        // to check 1 item in the response list
        Assert.assertEquals(1,schoolListViewModel.liveDataSchoolsList.getValue().size());
        // to check no error in loading response
        Assert.assertEquals("", schoolListViewModel.liveDataErrorMessage.getValue());
        // to check loading progress is false
        Assert.assertEquals(false, schoolListViewModel.liveDataLoadingProgress.getValue());
    }

    @Test
    public void getSchoolsListFailure(){


        testSingle = Single.error(new Throwable("Error Occurred"));

        Mockito.when(schoolRepository.getSchoolsList()).thenReturn(testSingle);

        schoolListViewModel.refresh();


        // to check no error in loading response
        Assert.assertEquals("Error Occurred", schoolListViewModel.liveDataErrorMessage.getValue());
        // to check loading progress is false
        Assert.assertEquals(false, schoolListViewModel.liveDataLoadingProgress.getValue());
    }
}
