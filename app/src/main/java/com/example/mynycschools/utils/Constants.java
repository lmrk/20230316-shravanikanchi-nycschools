package com.example.mynycschools.utils;

public interface Constants {

    String BUNDLE_DETAILS = "NYCSchoolDetails";
    String NO_DATA_FOR_SCHOOL = "Data not available for selected school";

}
