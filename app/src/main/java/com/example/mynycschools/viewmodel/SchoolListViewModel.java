package com.example.mynycschools.viewmodel;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.mynycschools.models.NYCSchool;
import com.example.mynycschools.models.NYCSchoolDetails;
import com.example.mynycschools.repository.SchoolRepository;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.annotations.NonNull;
import io.reactivex.rxjava3.disposables.CompositeDisposable;
import io.reactivex.rxjava3.observers.DisposableSingleObserver;
import io.reactivex.rxjava3.schedulers.Schedulers;

/**

 SchoolListViewModel - a ViewModel class responsible for holding and managing UI-related data for the School List screen.
 This class interacts with SchoolRepository to fetch data from the API and provides LiveData objects to update the UI.

 */
public class SchoolListViewModel extends ViewModel {
    @Inject
    SchoolRepository schoolRepository;
    public MutableLiveData<List<NYCSchool>> liveDataSchoolsList = new MutableLiveData<>();
    public MutableLiveData<String> liveDataErrorMessage = new MutableLiveData<>();
    public MutableLiveData<Boolean> liveDataLoadingProgress = new MutableLiveData<>();

    public MutableLiveData<List<NYCSchoolDetails>> liveDataSchoolDetail = new MutableLiveData<>();
    private CompositeDisposable disposable = new CompositeDisposable();


    @Inject
    public SchoolListViewModel(SchoolRepository schoolRepository) {
        this.schoolRepository = schoolRepository;
    }


    public void refresh() {
        fetchSchoolList();
    }

    public void onComplete() {
        liveDataSchoolDetail.setValue(null);
    }

    private void fetchSchoolList() {

        liveDataLoadingProgress.setValue(true);
        disposable.add(schoolRepository.getSchoolsList()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<List<NYCSchool>>() {

                    @Override
                    public void onSuccess(@NonNull List<NYCSchool> nycSchools) {
                        liveDataSchoolsList.setValue(nycSchools);
                        liveDataErrorMessage.setValue("");
                        liveDataLoadingProgress.setValue(false);
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        liveDataErrorMessage.setValue(e.getMessage());
                        liveDataLoadingProgress.setValue(false);
                        e.printStackTrace();
                    }
                })
        );
    }

    public void fetchSchoolDetails(String dbn) {

        liveDataLoadingProgress.setValue(true);
        disposable.add(schoolRepository.getSchoolDetails(dbn)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<List<NYCSchoolDetails>>() {
                    @Override
                    public void onSuccess(@NonNull List<NYCSchoolDetails> schoolDetails) {
                        liveDataSchoolDetail.setValue(schoolDetails);
                        liveDataErrorMessage.setValue("");
                        liveDataLoadingProgress.setValue(false);
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        liveDataErrorMessage.setValue(e.getMessage());
                        liveDataLoadingProgress.setValue(false);
                        e.printStackTrace();
                    }
                })
        );
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        disposable.clear();
    }
}
