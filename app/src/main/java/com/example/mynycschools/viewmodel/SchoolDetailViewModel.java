package com.example.mynycschools.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import com.example.mynycschools.models.NYCSchoolDetails;
import com.example.mynycschools.repository.SchoolRepository;

import javax.inject.Inject;

/**
 ViewModel class for displaying details of a single NYC school. This class retrieves school details
 using SchoolRepository and provides the data to the UI through LiveData.

 */
public class SchoolDetailViewModel extends ViewModel {

    private SchoolRepository schoolRepository;
    private LiveData<NYCSchoolDetails> nycSchoolDetailLiveData;

    @Inject
    public SchoolDetailViewModel(SchoolRepository schoolRepository) {
        this.schoolRepository = schoolRepository;
    }

    public LiveData<NYCSchoolDetails> getNYCSchoolDetailLiveData() {
        return nycSchoolDetailLiveData;
    }
}
