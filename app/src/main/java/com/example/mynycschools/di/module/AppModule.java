package com.example.mynycschools.di.module;

import android.app.Application;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**

 AppModule is a Dagger module class that provides the Application instance
 to the dependency graph.

 This module provides a single instance of the Application
 object throughout the application lifecycle.
 */
@Module
public class AppModule {

    private final Application mApplication;

    public AppModule(Application application){
        mApplication = application;
    }

    @Provides
    @Singleton
    public Application provideApplication(){
        return mApplication;
    }
}
