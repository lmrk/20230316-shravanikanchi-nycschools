package com.example.mynycschools.di.module;


import com.example.mynycschools.retrofit.SchoolAPIService;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

/**

 This module is responsible for providing the SchoolAPIService instance which is required for API calls related
 to school data retrieval.

 This module is dependent on NetworkModule, as it uses the Retrofit instance provided
 by NetworkModule to create the SchoolAPIService instance.

 */
@Module(includes = NetworkModule.class)
public class MainModule {

    @Provides
    @Singleton
    public SchoolAPIService provideSchoolAPIService(Retrofit retrofit){
        return retrofit.create(SchoolAPIService.class);
    }

}
