package com.example.mynycschools.di.component;

import androidx.lifecycle.ViewModelProvider;

import com.example.mynycschools.MainActivity;
import com.example.mynycschools.di.module.AppModule;
import com.example.mynycschools.di.module.MainModule;
import com.example.mynycschools.di.module.NetworkModule;
import com.example.mynycschools.di.module.ViewModelModule;
import com.example.mynycschools.repository.SchoolRepository;

import javax.inject.Singleton;

import dagger.Component;

/**

 Dagger component interface that defines the dependencies to be injected into the application.

 Uses @Singleton annotation to ensure only one instance of the component is created and used throughout
 the application lifecycle.

 Defines the modules to be used to provide the dependencies, including AppModule, NetworkModule, MainModule,
 and ViewModelModule.

 */
@Singleton
@Component(modules = {AppModule.class, NetworkModule.class, MainModule.class, ViewModelModule.class})
public interface AppComponent {

    ViewModelProvider.Factory viewModelFactory();

    void inject(MainActivity mainActivity);
    void inject(SchoolRepository schoolRepository);
}
