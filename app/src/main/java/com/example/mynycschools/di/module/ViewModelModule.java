package com.example.mynycschools.di.module;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.example.mynycschools.viewmodel.SchoolDetailViewModel;
import com.example.mynycschools.viewmodel.SchoolListViewModel;
import com.example.mynycschools.viewmodel.ViewModelFactory;
import com.example.mynycschools.viewmodel.ViewModelKey;

import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;

/**

 This module provides the ViewModel instances required by the application.

 It uses the @Binds annotation to bind each ViewModel implementation to the corresponding
 abstract ViewModel class or interface, and then adds the bindings to a Map using the @IntoMap
 annotation.

 Finally, it binds the ViewModelProvider.Factory implementation to the ViewModelFactory
 class.

 */
@Module
public abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(SchoolListViewModel.class)
    abstract ViewModel bindSchoolListViewModel(SchoolListViewModel viewModel);

    @Binds
    @IntoMap
    @ViewModelKey(SchoolDetailViewModel.class)
    abstract ViewModel bindSchoolDetailViewModel(SchoolDetailViewModel viewModel);

    @Binds
    abstract ViewModelProvider.Factory bindViewModelFactory(ViewModelFactory factory);

}
