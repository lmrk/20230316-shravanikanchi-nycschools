package com.example.mynycschools.fragments;

import com.example.mynycschools.models.NYCSchool;

/**
 * Clicklistener interface for item clicked in the NYC SchoolLIst Recyclerview
 *
 */
public interface OnItemClickListener {
    void onItemClicked(NYCSchool school);
}
