package com.example.mynycschools.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.mynycschools.R;
import com.example.mynycschools.adapters.SchoolsListAdapter;
import com.example.mynycschools.app.BaseApplication;
import com.example.mynycschools.models.NYCSchoolDetails;
import com.example.mynycschools.utils.Constants;
import com.example.mynycschools.viewmodel.SchoolListViewModel;
import com.google.android.material.snackbar.Snackbar;

import javax.inject.Inject;

/**
 * FragmentNYCSchools - Displays List of NYC Schools
 *
 */
public class FragmentNYCSchools extends Fragment implements Constants {

    private static final String TAG = "FragmentNYCSchools";

    @Inject
    ViewModelProvider.Factory viewModelFactory;

    @Inject
    SchoolListViewModel viewModelSchools;
    private SchoolsListAdapter schoolsListAdapter;
    private ProgressBar nProgressBar;
    private NavController navController;

    public FragmentNYCSchools(){
        super();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        viewModelFactory = ((BaseApplication) getActivity().getApplication())
                .getAppComponent().viewModelFactory();

        viewModelSchools = new ViewModelProvider(this, viewModelFactory).get(SchoolListViewModel.class);

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        this.navController = Navigation.findNavController(view);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        // Inflate View
        View view = inflater.inflate(R.layout.fragment_schools_list, container, false);
        nProgressBar = view.findViewById(R.id.progressBar_schoolList);

        // Init recycler view
        RecyclerView recyclerView = view.findViewById(R.id.recyclerview_schoolsList);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        // Setup adapter for recycler view
        schoolsListAdapter = new SchoolsListAdapter();
        recyclerView.setAdapter(schoolsListAdapter);

        // Adapter clicked
        schoolsListAdapter.setOnItemClickListener(school -> {
          //  getSchoolDetails(school.getDbn() , view);
            viewModelSchools.fetchSchoolDetails(school.getDbn());
        });

        // Get the list of schools data
        viewModelSchools.refresh();
        observeViewModel();

        return view;
    }

    private void observeViewModel(){

        viewModelSchools.liveDataSchoolsList.observe(getViewLifecycleOwner(), schoolList->{
            if(schoolList != null){
                schoolsListAdapter.updateSchoolsList(schoolList);
            }
        });

        viewModelSchools.liveDataLoadingProgress.observe(getViewLifecycleOwner(), isLoading-> {
            if(isLoading != null){
                nProgressBar.setVisibility(isLoading ? View.VISIBLE : View.GONE);
            }
        });

        viewModelSchools.liveDataErrorMessage.observe(getViewLifecycleOwner(), errorMessage ->{
            if(errorMessage != null){
                Snackbar snackbar = Snackbar.make(requireView(),errorMessage,Snackbar.LENGTH_SHORT);
                snackbar.show();
            }
        });

        viewModelSchools.liveDataSchoolDetail.observe(getViewLifecycleOwner(), schoolDetails ->{
            if(schoolDetails != null){
                if(schoolDetails.size() > 0){
                    NYCSchoolDetails nycSchoolDetail = schoolDetails.get(0);
                    Bundle bundle = new Bundle();
                    bundle.putParcelable(BUNDLE_DETAILS, nycSchoolDetail);

                    // Navigate to details fragment
                    navController.navigate(R.id.action_fragmentNYCSchoolsList_to_fragmentNYCSchoolDetails, bundle);
                    viewModelSchools.onComplete();
                }
                else{

                    Snackbar snackbar = Snackbar.make(requireView(),NO_DATA_FOR_SCHOOL,Snackbar.LENGTH_SHORT);
                    snackbar.show();
                }
            }
        });
    }


}
