package com.example.mynycschools.fragments;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProvider;

import com.example.mynycschools.R;
import com.example.mynycschools.app.BaseApplication;
import com.example.mynycschools.models.NYCSchoolDetails;
import com.example.mynycschools.viewmodel.SchoolDetailViewModel;

import javax.inject.Inject;

/**
 * FragmentNYCSchoolDetails - Displays the SAT score details of the selected school
 * for the school list screen

 */
public class FragmentNYCSchoolDetails extends BaseFragment {

    private static final String TAG = "FragmentNYCSchoolDetail";

    TextView tvSchoolName;

    TextView tvSatTestTakersCount;
    TextView tvSatCriticalReadingAvgScore;
    TextView tvSatMathAverageScore;
    TextView tvSatWritingAvgScore;

    @Inject
    ViewModelProvider.Factory viewModelFactory;

    @Inject
    SchoolDetailViewModel schoolDetailViewModel;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        viewModelFactory = ((BaseApplication) getActivity().getApplication())
                .getAppComponent().viewModelFactory();
        schoolDetailViewModel = new ViewModelProvider(this,viewModelFactory)
                .get(SchoolDetailViewModel.class);
    }


    private void setDetails(NYCSchoolDetails schoolDetails){

        tvSchoolName.setText(schoolDetails.getSchoolName());
        tvSatTestTakersCount.setText(String.valueOf(schoolDetails.getNumOfTestTakers()));
        tvSatCriticalReadingAvgScore.setText(String.valueOf(schoolDetails.getSatCriticalReadingAvgScore()));
        tvSatMathAverageScore.setText(String.valueOf(schoolDetails.getSatMathAvgScore()));
        tvSatWritingAvgScore.setText(String.valueOf(schoolDetails.getSatWritingAvgScore()));
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (getArguments() != null) {
            NYCSchoolDetails nycSchoolDetail = getArguments().getParcelable("NYCSchoolDetails");
            setDetails(nycSchoolDetail);
            Log.d(TAG, " nycSchoolDetail::test "+nycSchoolDetail.getSchoolName());
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        // Inflate View
        View view = inflater.inflate(R.layout.fragment_school_detail, container, false);

        tvSchoolName = view.findViewById(R.id.details_schoolName);
        tvSatTestTakersCount = view.findViewById(R.id.details_testTakersNum);
        tvSatCriticalReadingAvgScore = view.findViewById(R.id.details_readingAvg);
        tvSatMathAverageScore = view.findViewById(R.id.details_mathAvg);
        tvSatWritingAvgScore = view.findViewById(R.id.details_writingAvg);

        return view;
    }
}
