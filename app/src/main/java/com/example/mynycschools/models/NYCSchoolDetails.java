package com.example.mynycschools.models;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

/**
 * NYCSchool Details Model
 *
 */
public class NYCSchoolDetails implements Parcelable{

    @SerializedName("dbn")
    private String dbn;

    @SerializedName("school_name")
    private String schoolName;

    @SerializedName("num_of_sat_test_takers")
    private int numOfTestTakers;

    @SerializedName("sat_critical_reading_avg_score")
    private int satCriticalReadingAvgScore;

    @SerializedName("sat_math_avg_score")
    private int satMathAvgScore;

    @SerializedName("sat_writing_avg_score")
    private int satWritingAvgScore;


    public String getDbn() {
        return dbn;
    }

    public void setDbn(String dbn) {
        this.dbn = dbn;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    public int getNumOfTestTakers() {
        return numOfTestTakers;
    }

    public void setNumOfTestTakers(int numOfTestTakers) {
        this.numOfTestTakers = numOfTestTakers;
    }

    public int getSatCriticalReadingAvgScore() {
        return satCriticalReadingAvgScore;
    }

    public void setSatCriticalReadingAvgScore(int satCriticalReadingAvgScore) {
        this.satCriticalReadingAvgScore = satCriticalReadingAvgScore;
    }

    public int getSatMathAvgScore() {
        return satMathAvgScore;
    }

    public void setSatMathAvgScore(int satMathAvgScore) {
        this.satMathAvgScore = satMathAvgScore;
    }

    public int getSatWritingAvgScore() {
        return satWritingAvgScore;
    }

    public void setSatWritingAvgScore(int satWritingAvgScore) {
        this.satWritingAvgScore = satWritingAvgScore;
    }

    protected NYCSchoolDetails(Parcel in) {
        dbn = in.readString();
        schoolName = in.readString();
        numOfTestTakers = in.readInt();
        satCriticalReadingAvgScore = in.readInt();
        satMathAvgScore = in.readInt();
        satWritingAvgScore = in.readInt();
    }

    public static final Creator<NYCSchoolDetails> CREATOR = new Creator<NYCSchoolDetails>() {
        @Override
        public NYCSchoolDetails createFromParcel(Parcel in) {
            return new NYCSchoolDetails(in);
        }

        @Override
        public NYCSchoolDetails[] newArray(int size) {
            return new NYCSchoolDetails[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(@NonNull Parcel dest, int flags) {
        dest.writeString(dbn);
        dest.writeString(schoolName);
        dest.writeInt(numOfTestTakers);
        dest.writeInt(satCriticalReadingAvgScore);
        dest.writeInt(satMathAvgScore);
        dest.writeInt(satWritingAvgScore);
    }
}
