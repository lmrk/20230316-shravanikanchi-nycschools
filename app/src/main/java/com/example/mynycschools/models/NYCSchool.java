package com.example.mynycschools.models;

import com.google.gson.annotations.SerializedName;

/**
 * NYC School Model
 */
public class NYCSchool {

    @SerializedName("dbn")
    private String dbn;

    @SerializedName("school_name")
    private String schoolName;

    @SerializedName("boro")
    private String boro;

    @SerializedName("overview_paragraph")
    private String overviewParagraph;

    @SerializedName("campusName")
    private String campusName;

    @SerializedName("school_email")
    private String schoolEmail;

    @SerializedName("location")
    private String location;

    public String getDbn() {
        return dbn;
    }

    public void setDbn(String dbn) {
        this.dbn = dbn;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    public String getBoro() {
        return boro;
    }

    public void setBoro(String boro) {
        this.boro = boro;
    }

    public String getOverviewParagraph() {
        return overviewParagraph;
    }

    public void setOverviewParagraph(String overviewParagraph) {
        this.overviewParagraph = overviewParagraph;
    }

    public String getCampusName() {
        return campusName;
    }

    public void setCampusName(String campusName) {
        this.campusName = campusName;
    }

    public String getSchoolEmail() {
        return schoolEmail;
    }

    public void setSchoolEmail(String schoolEmail) {
        this.schoolEmail = schoolEmail;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public NYCSchool(String dbn, String schoolName, String boro, String overviewParagraph, String campusName, String schoolEmail, String location) {
        this.dbn = dbn;
        this.schoolName = schoolName;
        this.boro = boro;
        this.overviewParagraph = overviewParagraph;
        this.campusName = campusName;
        this.schoolEmail = schoolEmail;
        this.location = location;
    }
}
