package com.example.mynycschools.retrofit;

import com.example.mynycschools.models.NYCSchool;
import com.example.mynycschools.models.NYCSchoolDetails;

import java.util.List;

import io.reactivex.rxjava3.core.Single;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 SchoolAPIService interface provides the API endpoints to retrieve data from the NYC Open Data API
*/
public interface SchoolAPIService {


    @GET("s3k6-pzi2.json")
    Single<List<NYCSchool>> getSchoolsList();

    @GET("f9bf-2cp4.json")
    Single<List<NYCSchoolDetails>> getSchoolDetails(@Query("dbn") String dbn);

}
