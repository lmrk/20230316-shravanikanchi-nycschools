package com.example.mynycschools.repository;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;

import com.example.mynycschools.models.NYCSchool;
import com.example.mynycschools.models.NYCSchoolDetails;
import com.example.mynycschools.retrofit.SchoolAPIService;

import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.rxjava3.core.Single;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
/**

 SchoolRepository - responsible for fetching data from the network using the
 SchoolAPIService and returning it to the ViewModel as LiveData.

 It provides methods to fetch the list of NYCSchools and their details.

 It uses Retrofit to make network calls and returns
 LiveData objects containing the data or error messages if an error occurs.

 */

public class SchoolRepository {
    private SchoolAPIService schoolAPIService;

    @Inject
    public SchoolRepository(SchoolAPIService schoolAPIService){
        this.schoolAPIService = schoolAPIService;
    }

    public Single<List<NYCSchool>> getSchoolsList(){
        return schoolAPIService.getSchoolsList();
    }

    public Single<List<NYCSchoolDetails>> getSchoolDetails(String dbn){
        return schoolAPIService.getSchoolDetails(dbn);
    }

}
