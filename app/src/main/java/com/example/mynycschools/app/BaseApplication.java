package com.example.mynycschools.app;

import android.app.Application;

import com.example.mynycschools.di.component.AppComponent;
import com.example.mynycschools.di.component.DaggerAppComponent;
import com.example.mynycschools.di.module.AppModule;
import com.example.mynycschools.di.module.MainModule;
import com.example.mynycschools.di.module.NetworkModule;

/**

 -The BaseApplication class is the entry point for the application.

 -It extends the Application class and provides the
 instance of the Dagger AppComponent, which is responsible for creating and injecting dependencies.

 -The class initializes the Dagger components and modules and provides a public getter method to access the AppComponent.

 @author [Shravani Kanchi]
 @version 1.0
 @since [2020-03-16]
 */
public class BaseApplication extends Application {

    private String baseURL = " https://data.cityofnewyork.us/resource/";
    private AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();

       appComponent = DaggerAppComponent
                .builder()
                .networkModule(new NetworkModule(baseURL))
                .build();
    }

    public AppComponent getAppComponent(){
        return  appComponent;
    }
}
