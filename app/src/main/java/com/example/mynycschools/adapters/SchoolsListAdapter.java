package com.example.mynycschools.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.mynycschools.R;
import com.example.mynycschools.fragments.OnItemClickListener;
import com.example.mynycschools.models.NYCSchool;

import java.util.ArrayList;
import java.util.List;

/**
 * Adapter class - for NYCSchoolList RecyclerView
 */
public class SchoolsListAdapter extends
        RecyclerView.Adapter<SchoolsListAdapter.SchoolsListViewHolder>{

    private List<NYCSchool> schoolsList = new ArrayList<>();
    private OnItemClickListener onItemClickListener;

    public SchoolsListAdapter(){

    }

    public void updateSchoolsList(List<NYCSchool> newSchoolsList){
        schoolsList.clear();
        schoolsList.addAll(newSchoolsList);
        notifyDataSetChanged();
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener){
        this.onItemClickListener = onItemClickListener;
    }

    @NonNull
    @Override
    public SchoolsListAdapter.SchoolsListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View schoolView = inflater.inflate(R.layout.schools_item_view,parent,false);
        return new SchoolsListViewHolder(schoolView);
    }

    @Override
    public void onBindViewHolder(@NonNull SchoolsListAdapter.SchoolsListViewHolder holder, int position) {

        if(schoolsList.size() > 0) {
            NYCSchool nycSchool = schoolsList.get(position);
            holder.tvSchoolName.setText(nycSchool.getSchoolName());
            holder.tvSchoolInfo.setText(nycSchool.getOverviewParagraph());

            // format location String
            String location = nycSchool.getLocation();
            int endIndex = location.indexOf("("); // Get the index of the opening bracket
            String formattedLoc = location.substring(0, endIndex).trim();

            holder.tvLocation.setText(formattedLoc);
            holder.btnDetail.setOnClickListener(v -> {

            onItemClickListener.onItemClicked(schoolsList.get(position));
        });
        }
    }

    @Override
    public int getItemCount() {
        return schoolsList.size();
    }

    class SchoolsListViewHolder extends RecyclerView.ViewHolder{

        TextView tvSchoolName;
        TextView tvSchoolInfo;
        TextView tvLocation;

        Button btnDetail;

        public SchoolsListViewHolder(@NonNull View itemView) {
            super(itemView);

            tvLocation = itemView.findViewById(R.id.schoolListItem_location);
            tvSchoolName = itemView.findViewById(R.id.schoolListItem_schoolName);
            tvSchoolInfo = itemView.findViewById(R.id.schoolListItem_schoolInfo);
            btnDetail = itemView.findViewById(R.id.schoolListItem_detailsBtn);
        }
    }
}
